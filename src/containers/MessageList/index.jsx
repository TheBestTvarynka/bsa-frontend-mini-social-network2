import React, { useRef, useEffect } from 'react';
import Message from '../../components/Message/index';
import DaySeparator from '../../components/DaySeparator/index';

import './MessageList.css';

const Index = ({ messages, user, deleteMessage, toggleExpandedMessage, reactMessage }) => {
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  }
  useEffect(scrollToBottom, [messages]);

  // insert day separators between messages
  const messagesWithSeparators = [];
  for (let i = 0; i < messages.length - 1; i++) {
    messagesWithSeparators.push(messages[i]);
    const firstDate = new Date(messages[i].editedAt ? messages[i].editedAt : messages[i].createdAt);
    const secondDate = new Date(messages[i + 1].editedAt ? messages[i + 1].editedAt : messages[i + 1].createdAt);
    if (firstDate.getDate() !== secondDate.getDate()) {
      messagesWithSeparators.push({ id: i + '', date: secondDate.toDateString() })
    }
    if (messages[i].userId === messages[i + 1].userId) {
      messages[i].avatar = undefined;
    }
  }
  messagesWithSeparators.push(messages[messages.length - 1]);

  return (
    <div className="wrapper" id="message_list_wrapper">
      <div className="message_list">
        {messagesWithSeparators.map(message => (message.date
            ? <DaySeparator key={message.id} date={message.date} />
            : <Message key={message.id} data={message} user={user} deleteMessage={deleteMessage} updateMessage={toggleExpandedMessage} reactMessage={reactMessage}/>
        ))}
      </div>
      <div ref={messagesEndRef} />
    </div>
  );
}

export default Index;