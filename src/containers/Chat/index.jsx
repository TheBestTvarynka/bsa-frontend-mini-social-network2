import React from 'react';
import { connect } from 'react-redux';
import Messenger from '../Messenger/index';
import ChatDetails from '../ChatDetails/index';
import Index from '../Header/index';
import UpdateMessage from '../UpdateMessage/index';
import MediaSlider from '../../components/MediaSlider';

import './Chat.css';

const Chat = ({ expandedMessage, expandedMedia }) => {
  return (
    <div className="chat">
      <Index />
      <Messenger />
      <ChatDetails />
      {expandedMessage && <UpdateMessage />}
      {expandedMedia && <MediaSlider />}
    </div>
  );
}

const mapStateToProps = rootState => ({
  expandedMessage: rootState.expandedMessage,
  expandedMedia: rootState.expandedMedia
});

export default connect(
  mapStateToProps
)(Chat);