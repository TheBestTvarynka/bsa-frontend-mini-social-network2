import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addMessage, toggleExpandedMessage } from '../Messenger/actions';

import './UpdateMessage.css';

const Index = ({ message, addMessage, toggleExpandedMessage }) => {
  const { id, text: oldText } = message;
  const [text, setText] = useState(oldText);

  const handleCancel = () => {
    toggleExpandedMessage(undefined);
  };
  const handleUpdate = () => {
    addMessage({ id, text});
    setText('');
    handleCancel();
  };
  const handleClick = event => {
    if (event.target.id === 'background') {
      handleCancel();
    }
  };

  return (
    <div className="back" onClick={handleClick} id="background">
      <div className="update_message" id="modal_window">
        <span className="update_title">Edit message</span>
        <textarea value={text} onChange={ev => setText(ev.target.value)} />
        <div className="update_actions">
          <button className="cancel" onClick={handleCancel}>Cancel</button>
          <button className="update" onClick={handleUpdate}>Update</button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = rootState => ({
  message: rootState.expandedMessage
});

const actions = {
  addMessage,
  toggleExpandedMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);