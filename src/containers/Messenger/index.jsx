import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addMessage, deleteMessage, reactMessage, loadMessages, loadUser, toggleExpandedMessage } from './actions';

import Spinner from '../../components/Spinner/index';
import MessageList from '../MessageList/index';
import MessageInput from '../../components/MessageInput/index';

import './Messenger.css';

const Messenger = ({ messages, user, addMessage, deleteMessage, toggleExpandedMessage, reactMessage, loadMessages, loadUser }) => {
  if (!messages) loadMessages();
  if (!user) loadUser();

  return (
    <div className="messenger">
      {(messages === undefined || user === undefined)
      ? <Spinner />
      : <MessageList messages={messages} user={user} deleteMessage={deleteMessage} toggleExpandedMessage={toggleExpandedMessage} reactMessage={reactMessage}/>}
      <MessageInput sendMessage={addMessage} />
    </div>
  );
}

const mapStateToProps = rootState => ({
  messages: rootState.messages,
  user: rootState.user
});

const actions = {
  addMessage,
  deleteMessage,
  reactMessage,
  loadMessages,
  loadUser,
  toggleExpandedMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Messenger);