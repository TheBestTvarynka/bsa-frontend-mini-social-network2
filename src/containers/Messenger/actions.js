import {
  ADD_MESSAGE,
  SET_ALL_MESSAGES,
  SET_EXPANDED_MESSAGE,
  SET_EXPANDED_MEDIA,
  SET_USER,
  SET_ALL_USERS,
  SET_ALL_MEDIA,
  SET_ALL_FILES
} from './actionTypes';
import * as messageService from '../../services/messageService';
import { v4 as uuidv4 } from 'uuid';

const addMessageAction = message => ({
  type: ADD_MESSAGE,
  message
});

const setALLMessagesAction = messages => ({
  type: SET_ALL_MESSAGES,
  messages
});

const setUserAction = user => ({
  type: SET_USER,
  user
});

const setALLUsersAction = users => ({
  type: SET_ALL_USERS,
  users
});

const setALLMediaAction = media => ({
  type: SET_ALL_MEDIA,
  media
});

const setALLFilesAction = files => ({
  type: SET_ALL_FILES,
  files
});

const setExpandedMessageAction = expandedMessage => ({
  type: SET_EXPANDED_MESSAGE,
  expandedMessage
})

const setExpandedMediaAction = expandedMedia => ({
  type: SET_EXPANDED_MEDIA,
  expandedMedia
})

export const addMessage = newMessage => async (dispatch, getRootState) => {
  // here we send post request to backend server
  if (!newMessage.id) {
    // if id does not exists then we just add the message
    const { user } = getRootState();

    newMessage.id = uuidv4();
    newMessage.userId = user.id;
    newMessage.user = user.user;
    newMessage.avatar = user.url;
    newMessage.createdAt = new Date().toString();
    newMessage.editedAt = '';
    dispatch(addMessageAction(newMessage));
  } else {
    // else we update the message
    const { messages } = getRootState();
    const updated = messages.map(message => message.id === newMessage.id
      ? { ...message, text: newMessage.text, editedAt: new Date().toString() }
      : message);
    dispatch(setALLMessagesAction(updated));
  }
}

export const deleteMessage = messageId => async (dispatch, getRootState) => {
  const { messages } = getRootState();
  const updatedMessages = messages.filter(message => message.id !== messageId);
  dispatch(setALLMessagesAction(updatedMessages));
};

export const reactMessage = messageId => async (dispatch, getRootState) => {
  const { messages } = getRootState();
  const updatedMessages = messages.map(message => message.id === messageId
  ? { ...message, isLiked: !message.isLiked }
  : message);
  dispatch(setALLMessagesAction(updatedMessages));
};

export const loadMessages = () => async dispatch => {
  const messages = await messageService.loadMessages();
  dispatch(setALLMessagesAction(messages));
};

export const loadUser = () => async dispatch => {
  const user = messageService.loadUser();
  dispatch(setUserAction(user));
}

export const loadUsers = () => async dispatch => {
  const users = await messageService.loadUsers();
  dispatch(setALLUsersAction(users));
};

export const loadMedia = () => async dispatch => {
  const media = await messageService.loadMedia();
  dispatch(setALLMediaAction(media));
};

export const loadFiles = () => async dispatch => {
  const files = await messageService.loadFiles();
  dispatch(setALLFilesAction(files));
};

export const toggleExpandedMessage = message => async dispatch => {
  dispatch(setExpandedMessageAction(message));
}

export const toggleExpandedMedia = data => async dispatch => {
  dispatch(setExpandedMediaAction(data));
};

export const getLastUserMessage = () => (dispatch, getRootState) => {
  const { messages, user: { id: userId }} = getRootState();
  return messages.find(message => message.userId === userId);
}

export const formatDate = stringDate => {
  return new Date(stringDate.toString()).toLocaleTimeString();
};
