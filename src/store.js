import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './containers/Messenger/reducer.js';

import thunk from 'redux-thunk';

const initialState = {};

const middlewares = [ thunk ];
const composedEnhancers = compose(
  applyMiddleware(...middlewares)
);

const store = createStore(
  reducer,
  initialState,
  composedEnhancers
);

export default store;