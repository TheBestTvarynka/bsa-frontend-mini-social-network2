import React from 'react';
import { formatDate } from '../../containers/Messenger/actions';

import './Message.css';

const Message = ({ data, user, deleteMessage, updateMessage, reactMessage }) => {
  const { id, text, avatar, createdAt, editedAt, userId, isLiked, user: username } = data;

  return (
    <div className={(userId === user.id) ? 'message main' : 'message'}>
      {(userId !== user.id) &&
        (avatar ? <img src={avatar}  alt=""/> : <span style={{'minWidth': '40px'}} />)
      }
      <span className={(userId === user.id) ? 'message_text main_text' : 'message_text'}>
        <span className="username">{username}</span>
        {(userId === user.id) &&
          <span className="message_actions">
            <span onClick={() => updateMessage({id, text})}>Edit</span>
            <span onClick={() => deleteMessage(id)}>Delete</span>
          </span>
        }
        <span>{text}</span>
        <span className="date">{(editedAt ? 'edited ' : '') + formatDate(editedAt ? editedAt : createdAt)}</span>
      </span>
      {(userId !== user.id) &&
        (isLiked
          ? <img src="https://img.icons8.com/fluent/48/000000/filled-like.png"
                 alt=""
                 className="like"
                 onClick={() => reactMessage(id)}
          />
          : <img src="https://img.icons8.com/material-outlined/64/000000/filled-like.png"
                 alt=""
                 className="like"
                 onClick={() => reactMessage(id)}
          />)
      }
    </div>
  );
};

export default Message;