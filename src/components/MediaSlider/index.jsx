import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toggleExpandedMedia } from '../../containers/Messenger/actions';

import './MediaSlider.css';

const MediaSlider = ({ data, toggleExpandedMedia }) => {
  const [index, setIndex] = useState(data.index);
  const { media } = data;

  const handleNext = () => {
    if (index === media.length - 1) {
      return;
    }
    setIndex(index + 1);
  };
  const handlePrev = () => {
    if (index === 0) {
      return;
    }
    setIndex(index - 1);
  };
  const handleClick = event => {
    if (event.target.id === 'background' || event.target.id === 'slider_image' || event.target.id === 'slider') {
      toggleExpandedMedia(undefined);
    }
  };

  return (
    <div className="back" id="background" onClick={handleClick}>
      <div className="media_slider" id="slider">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
             width="32" height="32"
             viewBox="0 0 172 172"
             style={{"fill": "transparent"}}
             className="control_button"
             onClick={handlePrev}>
          <g fill="none" fillRule="nonzero" stroke="none" strokeWidth="1" strokeLinecap="butt" strokeLinejoin="miter" strokeMiterlimit="10" strokeDasharray="" strokeDashoffset="0" fontFamily="none" fontWeight="none" fontSize="none" textAnchor="none" style={{"mixBlendMode": "normal"}}>
            <path d="M0,172v-172h172v172z" fill="none" />
            <g fill="#cccccc">
              <path d="M86,16.125c-38.52783,0 -69.875,31.34717 -69.875,69.875c0,38.52783 31.34717,69.875 69.875,69.875c38.52783,0 69.875,-31.34717 69.875,-69.875c0,-38.52783 -31.34717,-69.875 -69.875,-69.875zM86,26.875c32.71192,0 59.125,26.41309 59.125,59.125c0,32.71192 -26.41308,59.125 -59.125,59.125c-32.71191,0 -59.125,-26.41308 -59.125,-59.125c0,-32.71191 26.41309,-59.125 59.125,-59.125zM95.57422,49.88672l-32.25,32.25l-3.69531,3.86328l3.69531,3.86328l32.25,32.25l7.72656,-7.72656l-28.38672,-28.38672l28.38672,-28.38672z" />
            </g>
          </g>
        </svg>
        <img src={media[index].url} alt="" className="slider_image" id="slider_image"/>
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
             width="32" height="32"
             viewBox="0 0 172 172"
             style={{"fill": "transparent"}}
             className="control_button"
             onClick={handleNext}>
          <g fill="none" fillRule="nonzero" stroke="none" strokeWidth="1" strokeLinecap="butt" strokeLinejoin="miter" strokeMiterlimit="10" strokeDasharray="" strokeDashoffset="0" fontFamily="none" fontWeight="none" fontSize="none" textAnchor="none" style={{"mixBlendMode": "normal"}}>
            <path d="M0,172v-172h172v172z" fill="none" />
            <g fill="#cccccc">
              <path d="M86,16.125c-38.52783,0 -69.875,31.34717 -69.875,69.875c0,38.52783 31.34717,69.875 69.875,69.875c38.52783,0 69.875,-31.34717 69.875,-69.875c0,-38.52783 -31.34717,-69.875 -69.875,-69.875zM86,26.875c32.71192,0 59.125,26.41309 59.125,59.125c0,32.71192 -26.41308,59.125 -59.125,59.125c-32.71191,0 -59.125,-26.41308 -59.125,-59.125c0,-32.71191 26.41309,-59.125 59.125,-59.125zM76.42578,49.88672l-7.72656,7.72656l28.38672,28.38672l-28.38672,28.38672l7.72656,7.72656l32.25,-32.25l3.69531,-3.86328l-3.69531,-3.86328z" />
            </g>
          </g>
        </svg>
      </div>
    </div>
  );
}

const mapStateToProps = rootState => ({
  data: rootState.expandedMedia
});

const actions = {
  toggleExpandedMedia
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MediaSlider);