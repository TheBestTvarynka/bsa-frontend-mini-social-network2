import React from 'react';

import './Spinner.css';

const Index = () => {
  return (
    <div className="spinner">
      <div className="loader"></div>
    </div>
  );
};

export default Index;