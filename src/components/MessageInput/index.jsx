import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLastUserMessage, toggleExpandedMessage } from '../../containers/Messenger/actions';

import './MessageInput.css';

const Index = ({ sendMessage, getLastUserMessage, toggleExpandedMessage }) => {
  const [text, setText] = useState('');

  const submit = () => {
    if (text === '') {
      return;
    }
    sendMessage({ text });
    setText('');
  };

  const handleKeyDown = event => {
    if (event.key === 'ArrowUp' && text === '') {
      const lastMessage = getLastUserMessage();
      if (!lastMessage) {
        return;
      }
      toggleExpandedMessage(lastMessage);
    }
    if (event.key !== 'Enter') {
      return;
    }
    submit();
  };

  return (
    <div className="message_input">
      <input placeholder="Type something..."
             onChange={ev => setText(ev.target.value)}
             value={text}
             onKeyDown={handleKeyDown}
      />
      <button onClick={submit}>Send</button>
    </div>
  );
};

const actions = {
  getLastUserMessage,
  toggleExpandedMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Index);